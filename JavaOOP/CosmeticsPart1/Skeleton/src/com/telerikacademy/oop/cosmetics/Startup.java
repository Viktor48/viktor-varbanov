package com.telerikacademy.oop.cosmetics;

import com.telerikacademy.oop.cosmetics.core.CosmeticsEngine;

public class Startup {
    
    public static void main(String[] args) {
        CosmeticsEngine engine = new CosmeticsEngine();
        engine.start();
    }
    
}
