package com.telerikacademy.oop.cosmetics.models.products;

import com.telerikacademy.oop.cosmetics.models.common.GenderType;

import java.util.Arrays;

public class Product {
    
    private double price;
    private String name;
    private String brand;
    private GenderType gender;
    
    public Product(String name, String brand, double price, GenderType gender) {
       setName(name);
       setBrand(brand);
       setPrice(price);
       setGender(gender);
    }




    private void setName(String name){
        if(name.length() < 3 || name.length() > 10){
            throw new IllegalArgumentException("Minimum product name’s length is 3 symbols and maximum is 10 symbols.");
        }
        this.name = name;
    }

    private void setBrand(String brand){
        if(brand.length() <2 || brand.length() > 10){
            throw new IllegalArgumentException("Minimum brand name’s length is 2 symbols and maximum is 10 symbols.");
        }
        this.brand = brand;
    }

    private void setPrice(double price){
        if(price <= 0){
            throw new IllegalArgumentException("Price cannot be negative.");
        }
        this.price = price;
    }

  private void setGender(GenderType type){
        this.gender = type;
  }


    public double getPrice() {return this.price;}
    public String print() {
        StringBuilder sb=  new StringBuilder();
        sb.append(String.format("#%s %s \n", this.name, this.brand));
        sb.append(String.format("#Price: $%.2f \n", this.price));
        sb.append(String.format("#Gender: %s \n", this.gender.toString()));
        sb.append("=== \n");

        return sb.toString().trim();
    }
    
}
