package com.telerikacademy.oop.cosmetics.models.cart;

import com.telerikacademy.oop.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    
    private List<Product> productList;
    
    public ShoppingCart() {
        this.productList= new ArrayList<>();
    }
    
    public List<Product> getProductList() {

        return this.productList;
    }
    
    public void addProduct(Product product) {

        if(product == null){
            throw new IllegalArgumentException("Product can't be null or empty!");
        }
        this.productList.add(product);
    }
    
    public void removeProduct(Product product) {
        if(product == null){
            throw new IllegalArgumentException("Product can't be null or empty!");
        }
        boolean isProductExist = this.productList.contains(product);
        if(!isProductExist){
            throw new IllegalArgumentException("Product doesn't exist!");
        }

        this.productList.remove(product);

    }
    
    public boolean containsProduct(Product product) {

        if(product == null){
            throw new IllegalArgumentException("Product can't be null!");
        }
        return this.productList.contains(product);
    }
    
    public double totalPrice() {

        double totalSum = 0;
        for (Product product:
             this.productList) {
            totalSum += product.getPrice();
        }

        return totalSum;
    }
    
}
