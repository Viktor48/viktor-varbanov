package com.telerikacademy.oop.cosmetics.core.factories;

import com.telerikacademy.oop.cosmetics.core.contracts.CosmeticsFactory;
import com.telerikacademy.oop.cosmetics.models.Category;
import com.telerikacademy.oop.cosmetics.models.cart.ShoppingCart;
import com.telerikacademy.oop.cosmetics.models.common.GenderType;
import com.telerikacademy.oop.cosmetics.models.products.Product;

public class CosmeticsFactoryImpl implements CosmeticsFactory {
    
    public Category createCategory(String name) {
        Category category = new Category(name);
        return category;
    }
    
    public Product createProduct(String name, String brand, double price, String gender) {

        GenderType defaultType = GenderType.UNISEX;
        int flag = 0;
        for (GenderType currentGender: GenderType.values()
             ) {
            if(currentGender.toString().equalsIgnoreCase(gender)){
                defaultType = currentGender;
                flag++;
            }

        }
        if(flag == 0){
            throw new IllegalArgumentException("Gender type can be Men, Women or Unisex.");
        }
       Product product = new Product(name,brand, price, defaultType);

        return product;
    }
    
    public ShoppingCart createShoppingCart() {
        ShoppingCart shoppingCart = new ShoppingCart();
        return shoppingCart;
    }
    
}
