package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class Category {
    
    private String name;
    private List<Product> products;
    
    public Category(String name) {
        setName(name);
        this.products = new ArrayList<Product>();
    }


    private void setName(String name){
        if(name.length() < 2 || name.length() > 15){
            throw new IllegalArgumentException("Minimum category name’s length length is 2 symbols and maximum is 15 symbols.");
        }
        this.name = name;
    }


    public List<Product> getProducts() {
        return this.products;
    }
    
    public void addProduct(Product product) {
        if(product == null){
            throw new IllegalArgumentException("Product can't be null or empty!");
        }
        this.products.add(product);
    }
    
    public void removeProduct(Product product) {
        if(product == null){
            throw new IllegalArgumentException("Product can't be null or empty!");
        }
       boolean isProductExist = this.products.contains(product);
       if(!isProductExist){
           throw new IllegalArgumentException("Product doesn't exist!");
       }
       this.products.remove(product);

    }
    
    public String print() {
       StringBuilder sb= new StringBuilder();
       sb.append(String.format("#Category: %s\n", this.name));
       if(this.products.size() != 0){
           for (Product product:
                   this.products  ) {
               sb.append(product.print());
           }
       }
        else{
            sb.append(" #No product in this category\n");
       }

        return sb.toString().trim();
    }
    
}
