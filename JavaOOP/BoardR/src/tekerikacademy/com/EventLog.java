package tekerikacademy.com;

import java.time.LocalDate;

import static tekerikacademy.com.Validation.*;

public class EventLog {
    private static final String DESCRIPTION_CANNOT_BE_NULL = "Description cannot be null!";
    private String description;

    private final LocalDate timestamp;

    public EventLog(String description) {
        setDescription(description);
        this.timestamp = LocalDate.now();
    }

    private void setDescription(String description) {
        if (!CoreValidation.IsObjectValid(description)) {
            throw new IllegalArgumentException(DESCRIPTION_CANNOT_BE_NULL);
        }
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public String viewInfo() {
        return String.format("[%s] %s\n", this.timestamp, this.description);
    }
}
