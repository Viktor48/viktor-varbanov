package tekerikacademy.com;

import java.time.LocalDate;
import java.util.ArrayList;

import static tekerikacademy.com.Validation.CoreValidation;

public abstract class BoardItem {
    private static final String TITLE_CANNOT_BE_NULL = "Title cannot be null!";
    private static final String DATE_CANNOT_BE_NULL = "Due date cannot be null!";
    private static final String DATE_CANNOT_BE_IN_THE_PAST = "Due date can't be set before today";

    private static final int MIN_TITLE_LENGTH = 5;
    private static final int MAX_TITLE_LENGTH = 30;
    private static final BoardStatus INITIAL_STATUS = BoardStatus.OPEN;

    private static final String TITLE_LENGTH_SIZE_ERROR = String.format("Please provide a title with length between %d and %d chars.", MIN_TITLE_LENGTH, MAX_TITLE_LENGTH);
    private static final String ITEM_CREATED = "Item created:  %s";
    private static final String TITLE_CHANGE = "Title changed from %s to %s";
    private static final String DATE_CHANGE = "Due date changed from %s to %s";
    protected static final String STATUS_CHANGE = "Status changed from %s to %s";
    private String title;
    private LocalDate dueDate;
    protected BoardStatus status;

    private boolean IsFromConstructor = true;

    protected ArrayList<EventLog> eventLogs;


    protected BoardItem(String title, LocalDate dueDate, BoardStatus status) {
        this.eventLogs = new ArrayList<EventLog>();
        setTitle(title);
        setDueDate(dueDate);
        setStatus(status);
        this.eventLogs.add(new EventLog(String.format(ITEM_CREATED, viewInfo())));

    }


    protected void setTitle(String title) {
        if (!CoreValidation.IsObjectValid(title)) {
            throw new IllegalArgumentException(TITLE_CANNOT_BE_NULL);
        }
        if (title.length() < MIN_TITLE_LENGTH || title.length() > MAX_TITLE_LENGTH) {
            throw new IllegalArgumentException(TITLE_LENGTH_SIZE_ERROR);
        }
        if (this.title != null) {
            this.eventLogs.add(new EventLog(String.format(TITLE_CHANGE, this.title, title)));
        }
        this.title = title;

    }

    public String getTitle() {
        return this.title;
    }

    protected void setDueDate(LocalDate date) {

        if (!CoreValidation.IsObjectValid(date)) {
            throw new IllegalArgumentException(DATE_CANNOT_BE_NULL);
        }
        if (!IsDateValid(date)) {
            throw new IllegalArgumentException(DATE_CANNOT_BE_IN_THE_PAST);
        }
        if (CoreValidation.IsObjectValid(dueDate)) {
            this.eventLogs.add(new EventLog(String.format(DATE_CHANGE, this.dueDate.toString(), date.toString())));
        }
        this.dueDate = date;
    }

    public LocalDate getDueDate() {
        return this.dueDate;
    }

    protected abstract void advanceStatus();

    protected void setStatus(BoardStatus status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status.toString();
    }

    protected abstract void revertStatus();

    public String viewInfo() {
        String result = String.format("\'%s\', [%s | %s]", this.title, this.status.toString(), this.dueDate);
        return result;
    }


    public String getHistory(){
        StringBuilder sb = new StringBuilder();
        for (EventLog event :
                eventLogs) {
            sb.append(event.viewInfo())
                    .append(System.lineSeparator());
        }

        return sb.toString();
    }


    private boolean IsDateValid(LocalDate date) {
        boolean isDateValid = date.isAfter(LocalDate.now());
        return isDateValid;
    }


}
