package tekerikacademy.com;


import java.time.LocalDate;

public class Issue extends  BoardItem{
    private static final String DEFAULT_DESCRIPTION_VALUE = "No description";
    private static final  String STATUS_CANNOT_BE_REVERTED = "Issue status already Open";
    private static final String STATUS_CANNOT_BE_MODIFIED = "Issue status already Verified";
    public static final String STATUS_SUCCESSFULLY_MODIFIED = "Issue status set to Verified";
    public static final String STATUS_SUCCESSFULLY_REVERTED = "Issue status set to Open";
    private static final BoardStatus INITIAL_STATUS=  BoardStatus.OPEN;
    private String description;
    public Issue(String title, String description, LocalDate dueDate) {
        super(title, dueDate, INITIAL_STATUS);
        setDescription(description);
    }

    private void setDescription(String description){
        if(Validation.CoreValidation.IsObjectValid(description)){
            this.description =description;
        }
        else{
            this.description = DEFAULT_DESCRIPTION_VALUE;
        }
    }

    @Override
    protected void advanceStatus() {
       if(this.status == BoardStatus.OPEN){
           this.status = BoardStatus.VERIFIED;
           eventLogs.add(new EventLog(STATUS_SUCCESSFULLY_MODIFIED));
       }
       else{
           this.eventLogs.add(new EventLog(STATUS_CANNOT_BE_MODIFIED));
       }
    }

    @Override
    protected void revertStatus() {
        if(status == BoardStatus.OPEN){
            eventLogs.add(new EventLog(STATUS_CANNOT_BE_REVERTED));
        }
        else{
            status = BoardStatus.OPEN;
            eventLogs.add(new EventLog(STATUS_SUCCESSFULLY_REVERTED));
        }
    }
    @Override
    public String viewInfo(){
        String base = super.viewInfo();



        return String.format("Issue: %s, %s", base, description);
    }
}
