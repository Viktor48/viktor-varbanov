package tekerikacademy.com;

import java.time.LocalDate;

import static tekerikacademy.com.Validation.*;

public class Task extends BoardItem {
    private static final String ASSIGNEE_CANNOT_BE_NULL = "Assignee cannot be null!";

    private static final int MIN_ASSIGNEE_LENGTH = 5;
    private static final int MAX_ASSIGNEE_LENGTH = 30;
    private static final String ASSIGNEE_VALUE_ERROR = String.format("Assignee name should be between %d and $d chars.", MIN_ASSIGNEE_LENGTH, MAX_ASSIGNEE_LENGTH);
    private static final String STATUS_CHANGED= "Task status changed from %s to %s";

    private static final String CHANGE_ASSIGNEE = "Assignee changed from %s to %s";
    private static final BoardStatus INITIAL_STATUS = BoardStatus.TODO;
    private String assignee;

    public Task(String title, String assignee, LocalDate dueDate) {
        super(title, dueDate, INITIAL_STATUS);
        setAssignee(assignee);

    }

    private void setAssignee(String value) {
        if (!CoreValidation.IsObjectValid(value)) {
            throw new IllegalArgumentException(ASSIGNEE_CANNOT_BE_NULL);
        }
        if (value.length() < MIN_ASSIGNEE_LENGTH || value.length() > MAX_ASSIGNEE_LENGTH) {
            throw new IllegalArgumentException(ASSIGNEE_VALUE_ERROR);
        }
        if (CoreValidation.IsObjectValid(assignee)) {
            eventLogs.add(new EventLog(String.format(CHANGE_ASSIGNEE, assignee, value)));
        }
        assignee = value;
    }

    public String getAssignee() {
        return assignee;
    }

    @Override
    protected void advanceStatus() {
        int ordinal = status.ordinal();
        BoardStatus nextStatus = status.getNext(ordinal);
        eventLogs.add(new EventLog(String.format(STATUS_CHANGED, status.toString(),nextStatus.toString())));
        status = nextStatus;
    }

    @Override
    protected void revertStatus() {
        int ordinal = status.ordinal();
        BoardStatus previousStatus = status.getPrevious(ordinal);
        eventLogs.add(new EventLog(String.format(STATUS_CHANGED, status.toString(),previousStatus.toString())));
        status = previousStatus;
    }

    @Override
    public String viewInfo(){
        String baseInfo = super.viewInfo();

        return  String.format("Task: %s, Assignee: %s", baseInfo, assignee);
    }
}
