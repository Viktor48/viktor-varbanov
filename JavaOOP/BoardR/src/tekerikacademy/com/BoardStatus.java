package tekerikacademy.com;

public enum BoardStatus {
    OPEN,
    TODO,
    IN_PROGRESS,
    DONE,
    VERIFIED;

    public BoardStatus getNext(int x) {
        if (x + 1 >= 4) {
            return VERIFIED;
        }
        else if (x + 1 == 1) {
            return TODO;
        } else if (x + 1 == 2) {
            return IN_PROGRESS;
        }
        return DONE;
    }

    public BoardStatus getPrevious(int x) {
        if (x - 1 <= 0) {
            return OPEN;
        } else if (x - 1 == 1) {
            return TODO;
        } else if (x - 1 == 2) {
            return IN_PROGRESS;
        }
        return DONE;
    }
}
