package tekerikacademy.com;

import java.util.*;


import static tekerikacademy.com.Validation.CoreValidation;

public class Board {
    private static final String ITEM_CANNOT_BE_NULL = "Item cannot be null!";
    private static final String ITEM_ALREADY_IN_THE_LIST = "Item is already in the collection!";
    private static List<BoardItem> boardItems = new ArrayList<>();



    public static void addItem(BoardItem item) {
        if (!CoreValidation.IsObjectValid(item)) {
            throw new IllegalArgumentException(ITEM_CANNOT_BE_NULL);
        }
        if (isItemExist(item)) {
            throw new IllegalArgumentException(ITEM_ALREADY_IN_THE_LIST);
        }
        boardItems.add(item);
    }

    private static boolean isItemExist(BoardItem item) {
        boolean isExist = boardItems.contains(item);
        return isExist;
    }

    public static int totalItems() {
        int numberOfElements = boardItems.size();
        return numberOfElements;
    }
    public static void displayHistory(Logger logger){

        for (BoardItem boardItem :
                boardItems) {
        logger.log(boardItem.getHistory());
        }
    }

}
