package tekerikacademy.com;

public class ConsoleLogger implements Logger {
    @Override
    public void log(String value) {
        System.out.print(value);
    }
}
