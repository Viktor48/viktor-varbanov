package tekerikacademy.com;

import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        // ReverseArray(sc);
        //SymmetricArrays(sc);
        // ThreeGroups(sc);
        //AboveTheMainDiagonal(sc);
        // LongestSequenceOfEqual(sc);
        //BigNumbers(sc);
        //Bounce(sc);
        SpiralMatrix(sc);

    }

    //Exercise 1 -  Reverse Array
    private static void ReverseArray(Scanner scanner) {
        String line = scanner.nextLine();
        String[] originalArray = line.split(" ");
        String[] reversedArray = new String[originalArray.length];
        for (int i = 0; i < originalArray.length; i++) {
            reversedArray[i] = originalArray[originalArray.length - 1 - i];
        }
        System.out.println(String.join(", ", reversedArray));
    }

    //Exercise 2 - Symmetric Arrays
    private static void SymmetricArrays(Scanner scanner) {
        int linesToRead = Integer.parseInt(scanner.nextLine());
        for (int i = 0; i < linesToRead; i++) {
            String[] arr = scanner.nextLine().split(" ");
            if (arr.length == 1) {
                System.out.println("Yes");
            } else {
                int length = arr.length / 2;
                boolean AreEqual = true;
                for (int j = 0; j < length; j++) {
                    if (arr[j].equals(arr[arr.length - 1 - j])) {
                    } else {
                        AreEqual = false;
                        break;
                    }
                }
                if (AreEqual) {
                    System.out.println("Yes");
                } else System.out.println("No");
            }
        }
    }

    //Exercise 3 - Three Groups
    private static void ThreeGroups(Scanner scanner) {
        int[] numbers = Arrays
                .stream(scanner.nextLine().split(" "))
                .mapToInt(n -> Integer.parseInt(n))
                .toArray();

        ArrayList<int[]> collection = new ArrayList<int[]>();

        int[] zeroReminderCollection = Arrays
                .stream(numbers)
                .filter(x -> x % 3 == 0)
                .toArray();

        collection.add(zeroReminderCollection);

        int[] oneReminderCollection = Arrays
                .stream(numbers)
                .filter(x -> x % 3 == 1)
                .toArray();
        collection.add(oneReminderCollection);

        int[] twoReminderCollection = Arrays
                .stream(numbers)
                .filter(x -> x % 3 == 2)
                .toArray();
        collection.add(twoReminderCollection);
        for (int i = 0; i < collection.size(); i++) {
            PrintResult(collection.get(i));
        }
    }

    //Method related to Exercise 3
    private static void PrintResult(int[] numbers) {
        if (numbers.length != 0) {
            String[] arr = Arrays.stream(numbers)
                    .mapToObj(String::valueOf)
                    .toArray(String[]::new);
            System.out.println(String.join(" ", arr));
        } else System.out.println();
    }

    //Exercise 4 - Above the Main Diagonal 2
    private static void AboveTheMainDiagonal(Scanner scanner) {
        int number = Integer.parseInt(scanner.nextLine());
        long[][] matrix = new long[number][number];
        long sum = 0;

        for (int row = 0; row < matrix.length; row++) {
            for (int col = row; col < matrix.length; col++) {
                sum += (long) Math.pow(2, row + col);
            }
        }
        System.out.println(sum);
    }

    // Exercise 5 - Longest Sequence of Equal
    private static void LongestSequenceOfEqual(Scanner scanner) {
        int length = Integer.parseInt(scanner.nextLine());
        int[] sequence = new int[length];
        for (int i = 0; i < length; i++) {
            sequence[i] = Integer.parseInt(scanner.nextLine());
        }
        int longestSequece = Integer.MIN_VALUE;
        for (int i = 0; i < length - 1; i++) {
            int currentSequence = 1;
            for (int j = i + 1; j < length; j++) {
                if (sequence[i] == sequence[j]) {
                    currentSequence++;
                } else break;
            }
            if (currentSequence > longestSequece) {
                longestSequece = currentSequence;
            }
        }
        System.out.println(longestSequece);
    }

    //Exercise 6 - Big Numbers
    private static void BigNumbers(Scanner sc) {
        Integer[] arraysSize = Arrays.stream(sc.nextLine().split(" "))
                .map(Integer::parseInt)
                .toArray(Integer[]::new);

        Integer minLength = 0;
        Integer maxLength = 0;
        boolean areEqual = false;
        boolean firstIsBigger = true;
        if (arraysSize[1] > arraysSize[0]) {
            minLength = arraysSize[0];
            maxLength = arraysSize[1];
            firstIsBigger = false;
        } else if (arraysSize[0] > arraysSize[1]) {
            minLength = arraysSize[1];
            maxLength = arraysSize[0];
        } else {
            minLength = arraysSize[0];
            maxLength = arraysSize[1];
            areEqual = true;
        }


        Integer[] firstArray = Arrays.stream(sc.nextLine().split(" "))
                .map(Integer::parseInt)
                .toArray(Integer[]::new);

        Integer[] secondArray = Arrays.stream(sc.nextLine().split(" "))
                .map(Integer::parseInt)
                .toArray(Integer[]::new);

        ArrayList<String> result = new ArrayList<String>();
        int addition = 0;
        for (int i = 0; i < minLength; i++) {
            int sum = 0;
            int numberToDisplay = 0;
            int firstNumber = firstArray[i];
            int secondNumber = secondArray[i];
            sum = firstNumber + secondNumber + addition;
            if (sum >= 10) {
                numberToDisplay = sum % 10;
                addition = 1;
            } else {
                numberToDisplay = sum;
                addition = 0;
            }
            result.add(String.valueOf(numberToDisplay));
        }
        if (!areEqual) {
            if (firstIsBigger) {

                for (int i = minLength; i < maxLength; i++) {
                    int sum = 0;
                    int numberToDisplay = 0;
                    sum = firstArray[i] + addition;
                    if (sum >= 10) {
                        numberToDisplay = sum % 10;
                        addition = 1;
                    } else {
                        numberToDisplay = sum;
                        addition = 0;
                    }
                    result.add(String.valueOf(numberToDisplay));

                }
                if (addition != 0) {
                    result.add(String.valueOf(addition));
                }

            } else {
                for (int i = minLength; i < maxLength; i++) {
                    int sum = 0;
                    int numberToDisplay = 0;
                    sum = secondArray[i] + addition;
                    if (sum >= 10) {
                        numberToDisplay = sum % 10;
                        addition = 1;
                    } else {
                        numberToDisplay = sum;
                        addition = 0;
                    }
                    result.add(String.valueOf(numberToDisplay));

                }
                if (addition != 0) {
                    result.add(String.valueOf(addition));
                }
            }

        }
        System.out.println(String.join(" ", result));
    }

    //Exercise 7 - Bounce
    private static void Bounce(Scanner sc) {
        Integer[] matrixSize = Arrays
                .stream(sc.nextLine().split(" "))
                .map(Integer::parseInt)
                .toArray(Integer[]::new);

        Integer rows = matrixSize[0];
        Integer columns = matrixSize[1];

        long[][] matrix = new long[rows][columns];
        int matrixBase = 2;
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < columns; col++) {
                int exponent = row + col;
                matrix[row][col] = (long) Math.pow(matrixBase, exponent);
            }
        }
        if (rows == columns) {
            long sum = 0;
            for (int row = 0; row < rows; row++) {
                for (int col = row; col <= row; col++) {
                    sum += matrix[row][col];
                }
            }
            System.out.println(sum);
        } else {
            int[] upperLeftBorder = {0, 0};
            int[] upperRightBorder = {0, columns - 1};
            int[] lowerLeftBorder = {rows - 1, 0};
            int[] lowerRightBorder = {rows - 1, columns - 1};

            long sum = 0;
            boolean downRight = true;
            boolean downLeft = false;
            boolean upRight = false;
            boolean upLeft = false;
            boolean borderFound = false;
            int currentRowPosition = 0;
            int currentColumnPosition = 0;
            while (!borderFound) {

                if (downRight) {
                    while (currentRowPosition < lowerRightBorder[0] && currentColumnPosition < lowerRightBorder[1]) {
                        sum += matrix[currentRowPosition][currentColumnPosition];
                        currentRowPosition++;
                        currentColumnPosition++;
                    }
                    if (currentRowPosition == lowerRightBorder[0] && currentColumnPosition == lowerRightBorder[1]) {
                        borderFound = true;
                    } else {
                        downRight = false;
                        if (currentRowPosition + 1 <= lowerLeftBorder[0]) {
                            downLeft = true;
                            //   currentRowPosition++;
                            //   currentColumnPosition--;
                        } else if (currentColumnPosition + 1 <= upperRightBorder[1]) {
                            upRight = true;


                        }
                    }
                } else if (downLeft) {
                    while (currentRowPosition < lowerLeftBorder[0] && currentColumnPosition > lowerLeftBorder[1]) {
                        sum += matrix[currentRowPosition][currentColumnPosition];
                        currentRowPosition++;
                        currentColumnPosition--;
                    }
                    if (currentRowPosition == lowerLeftBorder[0] && currentColumnPosition == lowerLeftBorder[1]) {
                        borderFound = true;
                    } else {
                        downLeft = false;
                        downRight = true;
                    }
                } else if (upRight) {
                    while (currentColumnPosition < upperRightBorder[1] && currentRowPosition > upperRightBorder[0]) {
                        sum += matrix[currentRowPosition][currentColumnPosition];
                        currentColumnPosition++;
                        currentRowPosition--;
                    }
                    if (currentColumnPosition == upperRightBorder[1] && currentRowPosition == upperRightBorder[0]) {
                        borderFound = true;
                    } else {
                        upRight = false;
                        upLeft = true;


                    }
                } else if (upLeft) {
                    while (currentRowPosition > upperLeftBorder[0] && currentColumnPosition >= upperLeftBorder[1]) {
                        sum += matrix[currentRowPosition][currentColumnPosition];
                        currentRowPosition--;
                        currentColumnPosition--;
                    }
                    if (currentRowPosition == upperLeftBorder[0] && currentColumnPosition == upperLeftBorder[1]) {
                        borderFound = true;
                    } else {
                        upLeft = false;
                        downLeft = true;
                    }
                }
            }
            sum += matrix[currentRowPosition][currentColumnPosition];
            System.out.println(sum);

        }
    }

    //Exercise 8 - SpiralMatrix
    private static void SpiralMatrix(Scanner scanner) {
        Integer matrixSize = Integer.parseInt(scanner.nextLine());
        int[][] matrix = new int[matrixSize][matrixSize];
        int number = 1;
        Integer numbersNeed = (int) Math.pow(matrixSize, 2);
        Integer topBorder = 0;
        Integer rightBorder = matrixSize - 1;
        Integer leftBorder = 0;
        Integer bottomBorder = matrixSize - 1;
        while (number <= numbersNeed) {
            for (int i = topBorder; i <= rightBorder; i++) {
                matrix[topBorder][i] = number;
                number++;
            }
            topBorder++;
            for (int i = topBorder; i <= bottomBorder; i++) {
                matrix[i][rightBorder] = number;
                number++;
            }
            rightBorder--;
            for (int i = rightBorder; i >= leftBorder; i--) {
                matrix[bottomBorder][i] = number;
                number++;
            }
            bottomBorder--;
            for (int i = bottomBorder; i >= topBorder; i--) {
                matrix[i][leftBorder] = number;
                number++;
            }
            leftBorder++;

        }
        for (int row = 0; row < matrixSize; row++) {
            for (int col = 0; col < matrixSize; col++) {
                System.out.printf("%d ", matrix[row][col]);
            }
            System.out.println();
        }
    }
}
