package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

public class TrainImpl  extends VehicleBase implements Train {

    private static final String INVALID_PASSENGER_CAPACITY = "A train cannot have less than 30 passengers or more than 150 passengers.";
    private static final String INVALID_CART_NUMBER = "A train cannot have less than 1 cart or more than 15 carts.";
    private static final int MINIMAL_PASSENGERS_CAPACITY = 30;
    private static final int MAXIMAL_PASSENGER_CAPACITY = 150;
    private static final int MINIMAL_CARTS_NUMBER = 1;
    private static final int MAXIMAL_CARTS_NUMBER = 15;
    private int carts;

    public TrainImpl(int passengerCapacity, double pricePerKilometer,int carts) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
        setCarts(carts);

    }

    private void setCarts(int carts) {
        if(carts < MINIMAL_CARTS_NUMBER || carts > MAXIMAL_CARTS_NUMBER){
            throw new IllegalArgumentException(INVALID_CART_NUMBER);
        }
        this.carts = carts;
    }

    @Override
    protected  void setPassengerCapacity(int passengerCapacity){
        if(passengerCapacity < MINIMAL_PASSENGERS_CAPACITY || passengerCapacity > MAXIMAL_PASSENGER_CAPACITY){
            throw new IllegalArgumentException(INVALID_PASSENGER_CAPACITY);
        }
        super.setPassengerCapacity(passengerCapacity);
    }
    @Override
    public int getCarts() {
        return carts;
    }


    @Override
    public String toString(){
        StringBuilder sb= new StringBuilder();
        sb.append(String.format("Train ----\n"));
        sb.append(super.toString());
        sb.append(String.format("Carts amount: %d\n", carts));

        return sb.toString();
    }
}
