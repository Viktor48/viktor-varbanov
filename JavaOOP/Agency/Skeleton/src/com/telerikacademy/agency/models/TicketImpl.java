package com.telerikacademy.agency.models;

import com.telerikacademy.agency.models.contracts.Journey;
import com.telerikacademy.agency.models.contracts.Ticket;

public class TicketImpl  implements Ticket {

    private  Journey journey;
    private  double administrativeCosts;

    public TicketImpl(Journey journey, double administrativeCosts) {
        this.journey = journey;
        this.administrativeCosts = administrativeCosts;
    }

    @Override
    public String print() {
        return null;
    }

    @Override
    public double getAdministrativeCosts() {
        return administrativeCosts;
    }

    @Override
    public Journey getJourney() {
        return journey;
    }

    @Override
    public double calculatePrice() {
        return 0;
    }

    @Override
    public String toString(){
        StringBuilder sb =new StringBuilder();
       /* Ticket ----
        Destination: VALUE
        Price: VALUE*/
        sb.append("Ticket ----\n");
        sb.append(String.format("Destination: %s\n", journey.getDestination()));
        sb.append(String.format("Price: %.2f\n", journey.calculateTravelCosts() * administrativeCosts));
         return sb.toString();
    }
}
