package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Airplane;

public class AirplaneImpl extends VehicleBase implements Airplane {

    private boolean hasFood;
    public AirplaneImpl(int passengerCapacity, double pricePerKilometer, boolean hasFood) {
        super(passengerCapacity, pricePerKilometer, VehicleType.AIR);
        this.hasFood = hasFood;
    }

    @Override
    public boolean hasFreeFood() {
        return hasFood;
    }



    @Override
    public String toString(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Airplane ----\n");
        stringBuilder.append(super.toString());
        stringBuilder.append(String.format("Has free food: %s\n", hasFood ));


        return stringBuilder.toString();
    }
}
