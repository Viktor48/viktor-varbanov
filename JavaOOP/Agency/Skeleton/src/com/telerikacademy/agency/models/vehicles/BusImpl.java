package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Bus;

public class BusImpl extends VehicleBase implements Bus {
private static final String INVALID_PASSENGER_NUMBER = "`A bus cannot have less than 10 passengers or more than 50 passengers.";
    private static final VehicleType INITIAL_TYPE= VehicleType.LAND;
    private static final int MINIMAL_PASSENGER_CAPACITY = 10;
    private static final int MAXIMAL_PASSENGER_CAPACITY = 50;
    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer, INITIAL_TYPE);
        setPassengerCapacity(passengerCapacity);
    }

    @Override
    protected void setPassengerCapacity(int passengerCapacity){
            if(passengerCapacity < MINIMAL_PASSENGER_CAPACITY || passengerCapacity > MAXIMAL_PASSENGER_CAPACITY){
                throw new IllegalArgumentException(INVALID_PASSENGER_NUMBER);
            }
            super.setPassengerCapacity(passengerCapacity);
    }


    @Override
    public String toString(){
        StringBuilder sb= new StringBuilder();
        sb.append("Bus ----\n");
        sb.append(super.toString());


        return sb.toString();
    }


}

