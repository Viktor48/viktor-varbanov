package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public class VehicleBase implements Vehicle {

    private static final String TYPE_CANT_BE_NULL = "Vehicle type cannot be null!";
    private static final  String INVALID_PASSENGER_CAPACITY ="A vehicle with less than 1 passengers or more than 800 passengers cannot exist!";
    private static final String INVALID_PRICE_PER_KILOMETER = "A vehicle with a price per kilometer lower than $0.10 or higher than $2.50 cannot exist!";

    private static final int MINIMAL_PASSENGER_CAPACITY = 1;
    private static final int MAXIMAL_PASSENGER_CAPACITY = 800;

    private static final  double MINIMAL_PRICE_PER_KILOMETER = 0.10;
    private static final double MAXIMAL_PRICE_PER_KILOMETER = 2.50;

    private int passengerCapacity;
    private double pricePerKilometer;
    private VehicleType type;
    
    public VehicleBase(int passengerCapacity, double pricePerKilometer, VehicleType type) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
        if (type == null)
            throw new IllegalArgumentException(TYPE_CANT_BE_NULL);
        this.type = type;
    }
    protected void setPassengerCapacity(int passengerCapacity) {
        if(passengerCapacity < MINIMAL_PASSENGER_CAPACITY || passengerCapacity > MAXIMAL_PASSENGER_CAPACITY){
            throw  new IllegalArgumentException(INVALID_PASSENGER_CAPACITY);
        }

        this.passengerCapacity= passengerCapacity;
    }

    protected void setPricePerKilometer(double pricePerKilometer) {
        if(pricePerKilometer < MINIMAL_PRICE_PER_KILOMETER || pricePerKilometer > MAXIMAL_PRICE_PER_KILOMETER){
            throw new IllegalArgumentException(INVALID_PRICE_PER_KILOMETER);
        }
            
        this.pricePerKilometer = pricePerKilometer;

    }

    @Override
    public String print() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("Passenger capacity: %d\n", passengerCapacity));
        sb.append(String.format("Price per kilometer: %d\n", pricePerKilometer));
        sb.append(String.format("Vehicle type: %s\n", type));

        return sb.toString();
    }

    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    public String printClassName(){
        return null;
    }

        @Override
    public String toString(){
            StringBuilder sb = new StringBuilder();
            sb.append(String.format("Passenger capacity: %d\n", passengerCapacity));
            sb.append(String.format("Price per kilometer: %.2f\n", pricePerKilometer));
            sb.append(String.format("Vehicle type: %s\n", type));

            return sb.toString();
        }

}
