package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.ScentType;
import com.telerikacademy.cosmetics.models.contracts.Cream;

public class CreamImpl extends ProductBase implements Cream {

    private ScentType scentType;

    public CreamImpl(String name, String brand, double price, GenderType gender, ScentType scent) {
        super(name, brand, price, gender);
        this.scentType = scent;

    }


    @Override
    public ScentType getScent() {
        return scentType;
    }

    @Override
    public String print() {
        String current = super.print();
        StringBuilder sb = new StringBuilder(current);
        sb.append(String.format(" #Scent: %s\n", scentType));

        return sb.toString();
    }
}
