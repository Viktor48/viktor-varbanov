package com.telerikacademy.cosmetics.models;


import com.telerikacademy.cosmetics.models.contracts.Category;
import com.telerikacademy.cosmetics.models.contracts.Product;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.cosmetics.models.common.Validation.*;

public class CategoryImpl implements Category {
    //use constants for validations values

    private String name;
    private List<Product> products;

    public CategoryImpl(String name) {
        setName(name);
        this.products = new ArrayList<>();
    }


    private void setName(String value) {
        if (isObjectNull(value)) {
            throw new IllegalArgumentException();
        }
        name = value;
    }

    public String getName() {
        return name;
    }

    public List<Product> getProducts() {
       //The reason for returning collection what copies original(internal class collection)
        // is related to security. New keyword is used to set a new place in heap with same values because we
        //copied the values.But if the client changes something in his collection these changes wil not modify
        //class's collecton because class collection point to different address in then heap.
        return new ArrayList<>(products);
    }

    public void addProduct(Product product) {
        if (isObjectNull(product)) {
            throw new IllegalArgumentException();
        }
        if(!products.contains(product)){
            products.add(product);
        }

    }

    public void removeProduct(Product product) {
        if (isObjectNull(product)) {
            throw new IllegalArgumentException();
        }

        products.remove(product);


    }


    public String print() {
        if (products.size() == 0) {
            return String.format("#Category: %s\n" +
                    " #No product in this category", name);
        }
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("#Category: %s\n", name));
        for (Product product :
                products) {
            sb.append(product.print());
            sb.append("===");
        }


       return sb.toString().trim();
    }

}
