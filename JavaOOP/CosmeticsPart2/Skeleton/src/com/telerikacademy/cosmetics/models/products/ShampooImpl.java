package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class ShampooImpl extends ProductBase implements Shampoo {

    private int milliliters;
    private UsageType usageType;
    public ShampooImpl(String name, String brand, double price, GenderType gender, int milliliters, UsageType everyDay) {
        super(name,brand,price,gender);
        setMilliliters(milliliters);
        this.usageType = everyDay;

    }
    private  void setMilliliters(int value){
        if(value < 0){
            throw  new IllegalArgumentException();
        }
        this.milliliters = value;
    }
    @Override
    public int getMilliliters() {
        return milliliters;
    }

    @Override
    public UsageType getUsage() {
        return usageType;
    }

    @Override
    public String print(){
        String current = super.print();
        StringBuilder sb = new StringBuilder(current);
        sb.append(String.format(" #Milliliters: %d\n", milliliters));
        sb.append(String.format(" #Usage: %s\n", usageType));

        return sb.toString();

    }
}
