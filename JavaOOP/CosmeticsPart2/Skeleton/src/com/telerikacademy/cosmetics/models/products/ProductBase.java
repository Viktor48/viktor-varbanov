package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;

import static com.telerikacademy.cosmetics.models.common.Validation.*;


public class ProductBase implements Product {
    private static final int MINIMAL_NAME_LENGTH = 3;
    private static final int MAXIMAL_VALUE_LENGTH = 10;
    private static final int MINIMAL_BRAND_LENGTH = 2;

    // name’s length is 3 symbols and maximum is 10 symbols.
    // brand name’s length is 2 symbols and maximum is 10 symbols.
    //Price cannot be negative.
    //Gender type can be "Men", "Women" or "Unisex".
    private String name;
    private String brand;
    private double price;
    private GenderType gender;

    protected ProductBase(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        this.gender = gender;

    }


    private void setName(String value) {
        if (isObjectNull(value)) {
            throw new IllegalArgumentException();
        }
        if (!isStringMatchesLengthCriteria(value, MINIMAL_NAME_LENGTH, MAXIMAL_VALUE_LENGTH)) {
            throw new IllegalArgumentException();
        }
        this.name = value;
    }

    private void setBrand(String value) {
        if (isObjectNull(value)) {
            throw new IllegalArgumentException();
        }
        if (!isStringMatchesLengthCriteria(value, MINIMAL_BRAND_LENGTH, MAXIMAL_VALUE_LENGTH)) {
            throw new IllegalArgumentException();
        }
        this.brand = value;
    }

    private void setPrice(double value) {
        if (value < 0) {
            throw new IllegalArgumentException();
        }
        this.price = value;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getBrand() {
        return brand;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public GenderType getGender() {
        return gender;
    }

    @Override
    public String print() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format(" #%s %s\n", name, brand));
        sb.append(String.format(" #Price: %.2f\n", price));
        sb.append(String.format(" #Gender: %s\n", gender));
        return sb.toString();

    }
}
