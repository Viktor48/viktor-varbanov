package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Toothpaste;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;
import static com.telerikacademy.cosmetics.models.common.Validation.*;
public class ToothpasteImpl extends ProductBase implements Toothpaste {


    private List<String> ingredients;
    public ToothpasteImpl(String name, String brand, double price, GenderType gender, List<String> ingredients)
    {
        super(name, brand, price, gender);
        setIngridients(ingredients);
    }

private void setIngridients(List<String> ingredients){
        if(isObjectNull(ingredients)){
            throw  new IllegalArgumentException();
        }
        this.ingredients = ingredients;
}
    @Override
    public List<String> getIngredients() {
        return ingredients;
    }

    @Override
    public String print(){
        String current = super.print();
        StringBuilder sb = new StringBuilder(current);
        sb.append(String.format(" #Ingredients: [%s]\n", String.join(", ", ingredients)));
        return sb.toString();
    }
}
