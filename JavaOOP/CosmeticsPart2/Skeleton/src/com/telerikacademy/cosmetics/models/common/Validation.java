package com.telerikacademy.cosmetics.models.common;

public class Validation {
    public static boolean isObjectNull(Object value){
        return value == null;
    }
    public static boolean isStringMatchesLengthCriteria(String value, int minLength, int maxLength){
        return value.length() >= minLength && value.length() <= maxLength;
    }
}
