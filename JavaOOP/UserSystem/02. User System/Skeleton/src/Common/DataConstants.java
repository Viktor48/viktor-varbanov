package Common;

public class DataConstants {
    public static class InputValidation{
        public static final int minimalUsernameLength = 3;

        public static final int minimalPasswordLength = 3;

        public static final int minimalInputArguments = 3;
    }

    public static class Operation{
        public static final String register = "register";
        public static final String delete = "delete";
        public static final String end = "end";
    }
    public static class DataValues{
        public static final int usernameIndex = 1;
        public static final int passwordIndex= 2;
    }

}
