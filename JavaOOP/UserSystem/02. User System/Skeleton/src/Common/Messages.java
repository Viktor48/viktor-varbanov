package Common;

public class Messages {
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_BLACK = "\u001B[30m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001B[32m";

    public static class SuccessfulMessages {
        public static final String userSuccessfullyRegistered = ANSI_GREEN + "Registered user." + ANSI_RESET;
        public static final String userSuccessfullyDeleted = ANSI_RED + "Deleted account." + ANSI_RESET;

    }

    public static class ErrorMessages {
        public static final String usernameNotLongEnough = ANSI_RED + "Username must be at least 3 characters long" + ANSI_RESET;
        public static final String passwordNotLongEnough = ANSI_RED + "Password must be at least 3 characters long." + ANSI_RESET;
        public static final String inputArgsNotEnough = ANSI_RED + "Too few parameters." + ANSI_RESET;

        public static final String usernameAlreadyExist = ANSI_RED + "Username already exists." + ANSI_RESET;
        public static final String systemError = ANSI_RED + "The system supports a maximum number of 4 users." + ANSI_RESET;
        public static final String userCredentialsInvalid = ANSI_RED + "Invalid account/password." + ANSI_RESET;
        public static final String invalidCommand = ANSI_RED + "Invalid operation" + ANSI_RESET;

    }
}
