package com.telerikacademy.core;

import java.util.Scanner;

import Common.DataConstants.InputValidation;
import Common.Messages.ErrorMessages;
import Common.Messages.SuccessfulMessages;
import Common.DataConstants.Operation;
import Common.DataConstants.DataValues;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String command = ReadInput(scanner);
        String[][] usersData = new String[4][2];
        // main loop
        while (!command.equals(Operation.end)) {
            String[] commandArgs = command.split(" ");
            switch (commandArgs[0]) {
                case Operation.register -> usersData = RegisterProcedure(commandArgs, usersData);
                case Operation.delete -> usersData = DeleteProcedure(commandArgs, usersData);
                default -> PrintMessage(ErrorMessages.invalidCommand);
            }
            // read next command
            command = ReadInput(scanner);
        }
    }

    private static String ReadInput(Scanner sc) {
        return sc.nextLine();
    }

    private static String[][] RegisterProcedure(String[] commandArgs, String[][] usersData) {
        //RegisterProcedure

        boolean isValid = DataValidationProcedure(commandArgs);
        if (isValid) {
            String username = GetData(commandArgs, DataValues.usernameIndex);
            String password = GetData(commandArgs, DataValues.passwordIndex);
            if (IsUserExist(usersData, username)) {
                PrintMessage(ErrorMessages.usernameAlreadyExist);
                isValid = false;

            }
            if (isValid) {
                int freeSlotIndex = FindFreeSlot(usersData);
                if (IsSlotValid(freeSlotIndex)) {
                    PrintMessage(ErrorMessages.systemError);
                } else {
                    usersData[freeSlotIndex][0] = username;
                    usersData[freeSlotIndex][1] = password;
                    PrintMessage(SuccessfulMessages.userSuccessfullyRegistered);
                }
            }
            return usersData;
        }
        return usersData;
    }

    private static String[][] DeleteProcedure(String[] commandArgs, String[][] usersData) {

        boolean isValid = DataValidationProcedure(commandArgs);
        String username = GetData(commandArgs, DataValues.usernameIndex);
        String password = GetData(commandArgs, DataValues.passwordIndex);


        if (isValid) {
            int accountIndex = FindUserToDelete(usersData, username, password);


            if (IsSlotValid(accountIndex)) {
                PrintMessage(ErrorMessages.userCredentialsInvalid);

            } else {
                usersData[accountIndex][0] = null;
                usersData[accountIndex][1] = null;

                PrintMessage(SuccessfulMessages.userSuccessfullyDeleted);
            }
        }
        return usersData;
    }

    private static boolean DataValidationProcedure(String[] commandArgs) {

        if (checkInputArguments(commandArgs)) {
            PrintMessage(ErrorMessages.inputArgsNotEnough);
            return false;
        }
        String username = GetData(commandArgs, DataValues.usernameIndex);
        String password = GetData(commandArgs, DataValues.passwordIndex);
        if (IsUsernameValid(username)) {
            PrintMessage(ErrorMessages.usernameNotLongEnough);
            return false;
        }
        if (IsPasswordValid(password)) {
            PrintMessage(ErrorMessages.passwordNotLongEnough);
            return false;
        }
        return true;
    }

    private static boolean checkInputArguments(String[] args) {
        return args.length < InputValidation.minimalInputArguments;
    }

    private static String GetData(String[] args, int index) {
        return args[index];
    }

    private static boolean IsUsernameValid(String username) {
        return username.length() < InputValidation.minimalUsernameLength;
    }

    private static boolean IsPasswordValid(String password) {
        return password.length() < InputValidation.minimalPasswordLength;
    }

    private static void PrintMessage(String message) {
        System.out.println(message);
    }

    private static boolean IsUserExist(String[][] usersData, String username) {
        boolean usernameExists = false;
        for (int i = 0, usersDataLength = usersData.length; i < usersDataLength; i++) {
            String[] usersDatum = usersData[i];
            if (username.equals(usersDatum[0])) {
                usernameExists = true;
                break;
            }
        }
        return usernameExists;
    }

    private static int FindFreeSlot(String[][] usersData) {
        for (int i = 0; i < usersData.length; i++) {
            if (usersData[i][0] == null)
                return i;
        }
        return -1;
    }

    private static int FindUserToDelete(String[][] usersData, String username, String password) {
        for (int i = 0; i < usersData.length; i++) {
            if (username.equals(usersData[i][0]) && password.equals(usersData[i][1]))
                return i;
        }
        return -1;
    }

    private static boolean IsSlotValid(int index) {
        return index == -1;
    }


}
