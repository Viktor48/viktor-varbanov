package com.telerikacademy;

import com.telerikacademy.datavalidation.DataValidation;

import java.time.LocalDate;

public class EventLog {
    private static final String DESCRIPTION_CANNOT_BE_NULL = "Description cannot be null!";
    private final String description;
    private final LocalDate timeStamp;

    public EventLog(String description){
        if(DataValidation.InputValidation.isObjectNull(description)){
            throw new IllegalArgumentException(DESCRIPTION_CANNOT_BE_NULL);
        }
        this.description = description;
        this.timeStamp = LocalDate.now();
    }
    public String getDescription(){
        return description;
    }

    public String viewInfo(){
        return String.format("[%s] %s", timeStamp, description);
    }
}
