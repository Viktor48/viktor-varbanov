package com.telerikacademy;

public enum BoardStatus {
    OPEN,
    TODO,
    INPROGRESS,
    DONE,
    VERIFIED;

    public BoardStatus getNext(int ordinal) {
        if (ordinal == 4 || ordinal == 3) {
            return VERIFIED;
        } else if (ordinal == 0) {
            return TODO;
        } else if (ordinal == 1) {
            return INPROGRESS;
        }
        return DONE;
    }

    public BoardStatus getPrevious(int ordinal) {
        if (ordinal == 0 || ordinal == 1) {
            return OPEN;
        } else if (ordinal == 2) {
            return TODO;
        } else if (ordinal == 3) {
            return INPROGRESS;
        }
        return DONE;
    }
}
