package com.telerikacademy.datavalidation;

import java.time.LocalDate;

public class DataValidation {

    public static class InputValidation {
        public static boolean isObjectNull(Object obj) {
            return obj == null;
        }
    }

    public static class ClassFieldsValidation {
        public static boolean isStringNotInRanges(String value, int minLength, int maxLength) {
            return value.length() < minLength || value.length() > maxLength;
        }

        public static boolean isStringEmpty(String value) {
            return value.equals("");
        }

        public static boolean isDateNotValid(LocalDate dueDate){
            return dueDate.isBefore(LocalDate.now());
        }
    }
}
