package com.telerikacademy;

import java.time.LocalDate;

import static com.telerikacademy.datavalidation.DataValidation.*;

public class BoardItem {
    private static final String TITLE_CANNOT_BE_NULL = "Title cannot be null!";
    private static final String TITLE_CANNOT_BE_EMPTY = "Title cannot be empty!";
    private static final BoardStatus INTIAl_STATE = BoardStatus.OPEN;

    private static final int MINIMAL_TITLE_LENGTH = 5;
    private static final int MAXIMAL_TITLE_LENGTH = 30;
    private String title;
    private LocalDate dueDate;
    private BoardStatus status;


    public BoardItem(String title, LocalDate dueDate) {
        setTitle(title);
        setDueDate(dueDate);
        this.status = INTIAl_STATE;
    }

    public  void setTitle(String value) {
        if (InputValidation.isObjectNull(value)) {
            throw new IllegalArgumentException(TITLE_CANNOT_BE_NULL);
        }
        if (ClassFieldsValidation.isStringEmpty(value)) {
            throw new IllegalArgumentException(TITLE_CANNOT_BE_EMPTY);
        }
        if (ClassFieldsValidation.isStringNotInRanges(value, MINIMAL_TITLE_LENGTH, MAXIMAL_TITLE_LENGTH)) {
            throw new IllegalArgumentException("Title length should be between ");
        }
        this.title = value;
    }

    public  void setDueDate(LocalDate dueDate){
        if(InputValidation.isObjectNull(dueDate)){
            throw new IllegalArgumentException();
        }
        if(ClassFieldsValidation.isDateNotValid(dueDate)){
            throw new IllegalArgumentException();
        }

        this.dueDate = dueDate;
    }

    public void revertStatus(){
        status = status.getPrevious(getStatusOrdinalValue());
    }

    public void advanceStatus(){
        status = status.getNext(getStatusOrdinalValue());
    }

    public String getStatus(){
        return status.toString();
    }

    private int getStatusOrdinalValue(){
        return status.ordinal();
    }

    public String viewInfo(){

        return String.format("\'%s\', [%s | %s]", this.title, getStatus(), dueDate);
    }

}
