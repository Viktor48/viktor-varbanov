import com.telerikacademy.BoardItem;
import com.telerikacademy.BoardStatus;
import org.junit.*;

import java.time.LocalDate;

public class BoardItemTests {

    //Arrange
    //Act
    //Assert
    @Test
    public void setNameShouldThrowExceptionIfNullValueIsPassed() {
        //Assert
        Assert.assertThrows(IllegalArgumentException.class, () -> {
            //Arrange && Act
            BoardItem item = new BoardItem(null, LocalDate.now());
        });
    }

    @Test
    public void setNameShouldThrowExceptionIfEmptyValueIsPassed() {
        //Assert
        Assert.assertThrows(IllegalArgumentException.class, () -> {
            //Arrange && Act
            BoardItem item = new BoardItem("", LocalDate.now());
        });
    }

    @Test
    public void setNameShouldThrowExceptionIfValueLengthIsSmallerThanExpected() {
        //Assert
        Assert.assertThrows(IllegalArgumentException.class, () -> {
            //Arrange && Act
            BoardItem item = new BoardItem("abvc", LocalDate.now());
        });
    }

    @Test
    public void setNameShouldThrowExceptionIfValueLengthIsBiggerThanExpected() {
        //Assert
        Assert.assertThrows(IllegalArgumentException.class, () -> {
            //Arrange && Act
            BoardItem item = new BoardItem("random text value passed to constructor", LocalDate.now());
        });
    }

    @Test
    public void setDueDateShouldThrowExceptionIfNullIsPassed(){
        //Assert
        Assert.assertThrows(IllegalArgumentException.class, () -> {
            //Arrange && Act
            BoardItem item = new BoardItem("random", null);
        });
    }

    @Test
    public void setDueDateShouldThrowExceptionIfDateInThePastIsPassed(){
        //Assert
        Assert.assertThrows(IllegalArgumentException.class, () -> {
            //Arrange && Act
            BoardItem item = new BoardItem("random", LocalDate.now().minusDays(1));
        });
    }

    @Test
    public void BoardItemShouldBeCreatedIfValidValuesArePassed(){
        //Arrange && Act
        BoardItem item = new BoardItem("Do the homework", LocalDate.now().plusDays(1));


        //Assert
        Assert.assertTrue(item instanceof BoardItem);
    }


    @Test
    public void BoardItemShouldHaveOPENAsInitialStatusWhenItIsInitialized()
    {
        //Arrange && Act
        BoardItem item = new BoardItem("Do the homework", LocalDate.now().plusDays(1));

        //Assert
        Assert.assertEquals(BoardStatus.OPEN.toString(), item.getStatus());
    }
    @Test
    public void advanceStatusShouldChangeTheClassStatusToNextStage(){

        //Arrange
        BoardItem item = new BoardItem("Do the homework", LocalDate.now().plusDays(1));

        //Act
        item.advanceStatus();
        //Assert
        Assert.assertEquals(BoardStatus.TODO.toString(), item.getStatus());
    }

    @Test
    public void advanceStatusShouldNotChangeStatusOnceItReachesTheMaximalValue(){
        //Arrange
        BoardItem item = new BoardItem("Do the homework", LocalDate.now().plusDays(1));
        //Act
        item.advanceStatus(); //OPEN,
        item.advanceStatus();//INPROGRESS
        item.advanceStatus();//DONE
        item.advanceStatus();//VERIFIED MAXIMAL VALUE
        item.advanceStatus();
        //Assert
        Assert.assertEquals(BoardStatus.VERIFIED.toString(), item.getStatus());
    }

    @Test
    public void revertStatusShouldNotChangeStatusOnceItReachesTheMinimumValue(){
        //Arrange
        BoardItem item = new BoardItem("Do the homework", LocalDate.now().plusDays(1));
        //Act
        item.revertStatus();
        //Assert
        Assert.assertEquals(BoardStatus.OPEN.toString(), item.getStatus());
    }

    @Test
    public void revertStatusShouldChangeStateToPreviousStage(){
        //Arrange
        BoardItem item = new BoardItem("Do the homework", LocalDate.now().plusDays(1));
        //Act
        item.advanceStatus();//TO_DO
        item.revertStatus();//OPEN
        //Assert
        Assert.assertEquals(BoardStatus.OPEN.toString(), item.getStatus());
    }


}
