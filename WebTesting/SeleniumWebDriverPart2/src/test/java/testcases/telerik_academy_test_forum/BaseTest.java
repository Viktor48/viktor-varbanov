package testcases.telerik_academy_test_forum;

import com.telerikacademy.testframework.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class BaseTest {
    UserActions actions = new UserActions();
    @BeforeClass
    public static void setUp(){
        UserActions.loadBrowser("telerikAcademyTestForum.mainPage");
    }

    @AfterClass
    public static void tearDown(){
        UserActions.quitDriver();
    }

}
