package testcases.telerik_academy_test_forum;
    import org.junit.Test;
import pages.telerik_academy_test_forum.MainPage;

public class MainPageTest extends BaseTest{

    @Test
    public void signInButtonShouldRedirectToLoginPage(){
        //ARRANGE
        MainPage mainPage = new MainPage(actions.getDriver());

        //ACT
        mainPage.clickSignInButton();

        //ASSERT
        actions.assertDynamicUrl("telerikAcademyTestForum.loginPage");
    }
}
