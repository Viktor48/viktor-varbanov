package testcases.telerik_academy_test_forum;

import com.telerikacademy.testframework.Utils;
import org.junit.Test;
import pages.telerik_academy_test_forum.LoginPage;

public class LoginPageTest extends BaseTest{

    public static final String EMAIL = "dummy12312@abv.bg";
    public static final String PASSWORD = "MJordan34";
    private static final String AVATAR_NAME = "Dummy12312";

    @Test
    public void SignInWithCustomAccountShouldRedirectToForumWhenAvailableCredentialsArePassed(){
        //ARRANGE
        LoginPage loginPage = new LoginPage(Utils.getWebDriver());
        loginPage.NavigateToPage();

        //ACT
        loginPage.EnterCredentials(EMAIL, PASSWORD);
        loginPage.signInWithCustomAccount();

        //ASSERT
        actions.assertLoggedUserIcon("telerikAcademyTestForum.mainPage.logged.avatarLink", AVATAR_NAME );

    }
}
