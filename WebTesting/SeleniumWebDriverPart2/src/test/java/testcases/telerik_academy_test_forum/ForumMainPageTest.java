package testcases.telerik_academy_test_forum;

import org.junit.Test;
import pages.telerik_academy_test_forum.ForumMainPage;

public class ForumMainPageTest extends BaseTest{

    //ENTER VALUES
    private static final String TITLE_TEXT = "Little pompai";
    private static final String BODY_TEXT = "Little pompai!";

    @Test
    public void createTopicShouldAddNewTopicToForum(){
        //ARRANGE
        ForumMainPage forumMainPage = new ForumMainPage(actions.getDriver());
        forumMainPage.navigateToPage();

        //ACT
        forumMainPage.clickAddTopicButton();
        forumMainPage.fillTopicTitle(TITLE_TEXT);
        forumMainPage.fillTopicBody(BODY_TEXT);
        forumMainPage.clickTagDropdown();
        forumMainPage.selectTag();
        forumMainPage.selectCategory();
        forumMainPage.clickCreateTopicButton();

        //ASSERT
        actions.assertElementPresent("telerikAcademyForum.main.newTopic.created");

    }

}
