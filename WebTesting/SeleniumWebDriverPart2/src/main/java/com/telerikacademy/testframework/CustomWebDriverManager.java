package com.telerikacademy.testframework;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import io.github.bonigarcia.wdm.managers.FirefoxDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

public class CustomWebDriverManager {
    public enum CustomWebDriverManagerEnum {
        INSTANCE;
        private WebDriver driver = setupBrowser();

        private WebDriver setupBrowser(){
            WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
            ChromeOptions chromeOptions = new ChromeOptions();
        //    chromeOptions.addArguments("headless");
            chromeOptions.addArguments("incognito");

            WebDriver chromeDriver = new ChromeDriver(chromeOptions);
            driver = chromeDriver;
            return chromeDriver;
        }

        public void quitDriver() {
            if (driver != null) {
                driver.quit();
                driver = null;
            }
        }

        public WebDriver getDriver() {
            if (driver == null){
                setupBrowser();
            }
            return driver;
        }


    }

}
