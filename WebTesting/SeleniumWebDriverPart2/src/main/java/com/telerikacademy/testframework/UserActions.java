package com.telerikacademy.testframework;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.security.Key;
import java.util.List;

public class UserActions {

    private static final int DEFAULT_TIMEOUT_IN_SECONDS = Integer.parseInt(Utils.getConfigPropertyByKey("defaultTimeOutInSeconds"));
    public WebDriver getDriver() {
        return driver;
    }

    final WebDriver driver;

    public UserActions() {
        this.driver = Utils.getWebDriver();
    }

    public static void loadBrowser(String baseUrlKey) {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey(baseUrlKey));
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }

    public void clickElement(String key){
        String locator = Utils.getUIMappingByKey(key);
        WebElement element = driver.findElement(By.xpath(locator));
        element.click();
    }



    public void typeInField(String key, String value){
        String locator = Utils.getUIMappingByKey(key);
        WebElement element = driver.findElement(By.xpath(locator));
        element.sendKeys(value);
    }

    public void waitForElementVisible(String key){
       String locator = Utils.getUIMappingByKey(key);
       WebElement webElement = driver.findElement(By.xpath(locator));
       WebDriverWait driverWait = new WebDriverWait(getDriver(),DEFAULT_TIMEOUT_IN_SECONDS);
       driverWait.until(ExpectedConditions.visibilityOf(webElement));
    }


    public boolean isElementPresent(String key){
        waitForElementVisible(key);
        WebElement webElement = driver.findElement(By.xpath(key));
        WebDriverWait driverWait = new WebDriverWait(getDriver(),DEFAULT_TIMEOUT_IN_SECONDS);
        if(webElement == null){
            return false;
        }
        return true;
    }


    public void assertNavigatedUrl(String urlKey) {
        String actualUrl = driver.getCurrentUrl();
        String expectedUrl = Utils.getConfigPropertyByKey(urlKey);
        Assert.assertEquals(expectedUrl, actualUrl);
    }

    public void assertDynamicUrl(String urlKey){
        String currentUrl = driver.getCurrentUrl();
        String urlToContain = Utils.getConfigPropertyByKey(urlKey);
        Assert.assertTrue(currentUrl.contains(urlToContain));
    }

    public void assertLoggedUserIcon(String urlKey, String expectedIconName){
            String locator = Utils.getUIMappingByKey(urlKey);
            WebElement element = driver.findElement(By.xpath(locator));
            String actualIconName= element.getAttribute("Title");
            Assert.assertEquals(expectedIconName, actualIconName);
    }

    public void assertElementPresent(String urlKey){
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(urlKey))));
    }



}
