package com.telerikacademy.page;

import com.telerikacademy.testframework.UserActions;
import com.telerikacademy.testframework.Utils;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public abstract class BasePage {

    protected String url;
    protected WebDriver webDriver;
    public UserActions actions;

    protected BasePage(WebDriver driver, String urlKey) {
        String pageUrl = Utils.getConfigPropertyByKey(urlKey);
        this.webDriver = driver;
        this.url = pageUrl;
        actions = new UserActions();
    }

    public String getUrl(){ return url; };

    public void navigateToPage(){
        this.webDriver.get(url);
    }

    public void clickHamburger(){
        actions.waitForElementVisible("telerikAcademyTestForum.mainPage.hamburger");
        actions.clickElement("telerikAcademyTestForum.mainPage.hamburger");
    }

    public void assertPageNavigated() {
        String currentUrl = webDriver.getCurrentUrl();
        Assert.assertTrue("Landed URL is not as expected. Actual URL: " + currentUrl + ". Expected URL: " + url, currentUrl.contains(url));
    }

}
