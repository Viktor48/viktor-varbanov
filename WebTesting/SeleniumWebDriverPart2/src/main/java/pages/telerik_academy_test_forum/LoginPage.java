package pages.telerik_academy_test_forum;

import com.telerikacademy.page.BasePage;
import org.openqa.selenium.WebDriver;
import org.sonatype.guice.bean.containers.Main;

public class LoginPage extends BasePage {

    public LoginPage(WebDriver driver) {
        super(driver, "telerikAcademyTestForum.mainPage");
    }

    public void NavigateToPage(){
        MainPage mainPage = new MainPage(actions.getDriver());
        mainPage.clickSignInButton();
    }

    public void EnterCredentials(String email, String password){
        actions.typeInField("telerikAcademyTestForum.loginPage.email", email);
        actions.typeInField("telerikAcademyTestForum.loginPage.password", password);
    }

    public void signInWithCustomAccount(){
        actions.clickElement("telerikAcademyTestForum.loginPage.singInButton");
    }

    public void signInWithTelerikAcademyAccount(){

    }
}
