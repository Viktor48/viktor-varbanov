package pages.telerik_academy_test_forum;

import com.telerikacademy.page.BasePage;
import org.openqa.selenium.WebDriver;

public class ForumMainPage extends BasePage {
    public ForumMainPage(WebDriver driver) {
        super(driver, "telerikAcademyTestForum.mainPage");
    }

    public void navigateToPage(){
        MainPage mainPage = new MainPage(actions.getDriver());
        mainPage.clickSignInButton();
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.EnterCredentials("dummy12312@abv.bg", "MJordan34");
        loginPage.signInWithCustomAccount();
    }

    public void clickAddTopicButton() {
        actions.waitForElementVisible("telerikAcademyTestForum.mainPage.newTopicButton");
        actions.clickElement("telerikAcademyTestForum.mainPage.newTopicButton");
        actions.waitForElementVisible("telerikAcademyTestForum.mainPage.newTopicWindow");
    }

    public void fillTopicTitle(String value){
        actions.waitForElementVisible("telerikAcademyForum.mainPage.newTopic.title");
        actions.typeInField("telerikAcademyForum.mainPage.newTopic.title", value);
    }

    public void fillTopicBody(String value){
        actions.typeInField("telerikAcademyForum.mainPage.newTopic.body", value);
    }

    public void clickTagDropdown(){
        actions.clickElement("telerikAcademyForum.mainPage.newTopic.tagDropdown");
    }

    public void selectTag() {
        actions.waitForElementVisible("telerikAcademyForum.mainPage.newTopic.tagDropdown.preparation");
        actions.clickElement("telerikAcademyForum.mainPage.newTopic.tagDropdown.preparation");

    }
    public void selectCategory(){
    actions.clickElement("telerikAcademyForum.mainPage.newTopic.categoryDropdown");
    actions.waitForElementVisible("telerikAcademyForum.mainPage.newTopic.categoryDropdown.alphaPrep");
    actions.clickElement("telerikAcademyForum.mainPage.newTopic.categoryDropdown.alphaPrep");
    }


    public void clickCreateTopicButton(){
        actions.clickElement("telerikAcademyForum.mainPage.newTopic.createTopicButton");
    }





}
