package pages.telerik_academy_test_forum;

import com.telerikacademy.page.BasePage;

import org.openqa.selenium.WebDriver;

public class MainPage extends BasePage {


    public MainPage(WebDriver driver) {
        super(driver, "telerikAcademyTestForum.mainPage");
    }

    public void clickSignInButton(){
        actions.clickElement("telerikAcademyTestForum.mainPage.signInButton");
    }

}
