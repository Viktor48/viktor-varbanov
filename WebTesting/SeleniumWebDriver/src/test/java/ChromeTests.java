import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class ChromeTests {
    private static final String username = "dummy12312@abv.bg";
    private static final String password = "MJordan34";
    WebDriver webDriver;

    @BeforeEach
    public void setUp() {
    //Change the path before testing
        System.setProperty("webdriver.chrome.driver", "D:\\Selenium drivers and jars\\drivers\\chromedriver\\chromedriver.exe");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("incognito");
        webDriver = new ChromeDriver(chromeOptions);
        webDriver.get("https://www.google.com/");
        webDriver.findElement(By.id("L2AGLb")).click();
        webDriver.get("https://stage-forum.telerikacademy.com/");

        webDriver.findElement(By.xpath("//span[@class ='d-button-label']")).click();
        Assertions.assertEquals("Sign in", webDriver.getTitle());

        webDriver.findElement(By.xpath("//input[@id='Email']")).sendKeys(username);
        webDriver.findElement(By.xpath("//input[@id='Password']")).sendKeys(password);
        webDriver.findElement(By.xpath("//button[@id='next']")).click();
        try {
            if (webDriver.findElement(By.xpath("//span[@class= 'd-button-label' and text() = 'Open Draft']")).isDisplayed()) {
                webDriver.findElement(By.xpath("//span[@class= 'd-button-label' and text() = 'Open Draft']")).click();
                webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.MILLISECONDS);
                webDriver.findElement(By.xpath("//*[@class='cancel']")).click();
            }
        } catch (Exception ex) {

        }

    }

    @AfterEach
    public void tearDown() {
        webDriver.close();
        webDriver.quit();
    }


    @Test
    public void AddTopicToForum() {
        WebElement newTopicButton = webDriver.findElement(By.xpath("//span[@class= 'd-button-label' and text() = \"New Topic\"]"));
        newTopicButton.click();
        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        WebElement titleField = webDriver.findElement(By.xpath("//input[@aria-label='Type title, or paste a link here']"));
        titleField.sendKeys("This is about dogs");
        WebElement categories = webDriver.findElement(By.xpath("//div[@class='select-kit-selected-name selected-name choice' and @title='optional tags']"));
        categories.click();
        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.MILLISECONDS);
        webDriver.findElement(By.xpath("//li[@title = 'preparation']")).click();
        webDriver.findElement(By.xpath("//*[@class='select-kit-header single-select-header combo-box-header ember-view']")).click();
        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.MILLISECONDS);
        webDriver.findElement(By.xpath("//*[@class='category-desc' and text() ='Welcome to the question space for your Alpha Program preparation. Here you can find materials to prepare yourself,  ask questions and browse in previously discussed topics and help your colleagues whi…']")).click();
        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.MILLISECONDS);
        webDriver.findElement(By.xpath("//textarea[@class='d-editor-input ember-text-area ember-view']")).sendKeys("Invalid data: Check system response when InValid  test data is submitted");
        webDriver.findElement(By.xpath("//span[@class='d-button-label' and text() = 'Create Topic']")).click();
        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.MILLISECONDS);

        Assertions.assertTrue(true);
    }

    @Test
    public void seeLatestPosts() {
        webDriver.findElement(By.xpath("//li[@title='topics with recent posts']")).click();

        Assertions.assertTrue(true);
    }

}
