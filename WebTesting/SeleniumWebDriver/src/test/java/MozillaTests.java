import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class MozillaTests {
    WebDriver webDriver;

    @BeforeEach
    public void setUp(){
        //Remember to change before running tests
        System.setProperty("webdriver.gecko.driver", "resources/geckodriver.exe");
      //  System.setProperty("webdriver.gecko.driver", "D:\\Selenium drivers and jars\\drivers\\moziladriver\\geckodriver.exe");
        webDriver = new FirefoxDriver();
        webDriver.get("https://www.google.com/");
        webDriver.findElement(By.id("L2AGLb")).click();

    }

    @AfterEach
    public void closeWebDriver(){
        webDriver.close();
    }

    @Test
    public void Test(){
        Assertions.assertTrue(true);
    }

}
