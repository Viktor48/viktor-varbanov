package pages.telerikAcademyTestForum;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class TelerikAcademyHomePage extends BasePage {


    public TelerikAcademyHomePage(WebDriver driver) {
        super(driver, "telerikAcademyTestForum.mainPage.url");
    }

    @Override
    public void navigateToPage() {
        super.navigateToPage();
        TelerikAcademyMainPage telerikAcademyMainPage = new TelerikAcademyMainPage(actions.getDriver());
        telerikAcademyMainPage.clickSignInButton();
        TelerikAcademyLoginPage telerikAcademyLoginPage = new TelerikAcademyLoginPage(actions.getDriver());
        telerikAcademyLoginPage.typeEmail("dummy12312@abv.bg");
        telerikAcademyLoginPage.typePassword("MJordan34");
        telerikAcademyLoginPage.clickSignInButton();
    }
    public void assertTopicVisible(){
        actions.assertElementPresent("telerikAcademyTestForum.mainPage.newTopicButton");
    }
    public void clickAddNewTopic(){
        actions.waitForElementVisible("telerikAcademyTestForum.mainPage.newTopicButton");
       actions.clickElement("telerikAcademyTestForum.mainPage.newTopicButton");
    }
    public void fillTopicTitle(String value){
        actions.waitForElementVisible("telerikAcademyTestForum.mainPage.newTopicWindow");
        actions.typeValueInField(value, "telerikAcademyForum.mainPage.newTopic.title");
    }

    public void fillTopicBody(String value){
        actions.waitForElementVisible("telerikAcademyForum.mainPage.newTopic.body");
        actions.typeValueInField(value,"telerikAcademyForum.mainPage.newTopic.body");
    }

    public void clickTagDropdown(){
        actions.waitForElementVisible("telerikAcademyForum.mainPage.newTopic.tagDropdown");
        actions.clickElement("telerikAcademyForum.mainPage.newTopic.tagDropdown");
    }

    public void selectTag() {
        actions.waitForElementVisible("telerikAcademyForum.mainPage.newTopic.tagDropdown.preparation");
        actions.clickElement("telerikAcademyForum.mainPage.newTopic.tagDropdown.preparation");

    }
    public void selectCategory(){
        actions.clickElement("telerikAcademyForum.mainPage.newTopic.categoryDropdown");
        actions.waitForElementVisible("telerikAcademyForum.mainPage.newTopic.categoryDropdown.alphaPreparation");
        actions.clickElement("telerikAcademyForum.mainPage.newTopic.categoryDropdown.alphaPreparation");
    }


    public void clickCreateTopicButton(){
        actions.clickElement("telerikAcademyForum.mainPage.newTopic.createTopicButton");
    }

    public void assertTopicCreated(){
        actions.waitForElementVisible("telerikAcademyForum.main.newTopic.created");
        actions.assertElementPresent("telerikAcademyForum.main.newTopic.created");
    }

}
