package pages.telerikAcademyTestForum;

import com.telerikacademy.testframework.Utils;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class TelerikAcademyLoginPage extends TelerikAcademyMainPage {

    public TelerikAcademyLoginPage(WebDriver driver) {
        super(driver);
    }

    public void assertEmailIsVisible() {
        actions.assertElementPresent("telerikAcademyTestForum.loginPage.email");
    }

    public void assertPasswordVisible() {
        actions.assertElementPresent("telerikAcademyTestForum.loginPage.password");
    }

    public void assertSignInButtonVisible() {
        actions.assertElementPresent("telerikAcademyTestForum.loginPage.signInButton");
    }

    public void typeEmail(String email) {
        actions.typeValueInField(email, "telerikAcademyTestForum.loginPage.email");
    }

    public void typePassword(String password) {
        actions.typeValueInField(password, "telerikAcademyTestForum.loginPage.password");
    }

    public void clickSignInButton() {
        actions.clickElement("telerikAcademyTestForum.loginPage.signInButton");
    }

    public void assertPageUrl() {
        String currentUrl = driver.getCurrentUrl();
        Assert.assertTrue(currentUrl.contains(Utils.getConfigPropertyByKey("telerikAcademyTestForum.loginPage.url")));
    }
}
