package pages.telerikAcademyTestForum;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class TelerikAcademyMainPage extends BasePage {

    public TelerikAcademyMainPage(WebDriver driver) {
        super(driver, "telerikAcademyTestForum.mainPage.url");
    }

    public void assertLogoIsVisible(){
        actions.assertElementPresent("telerikAcademyTestForum.mainPage.logo");
    }
    public void assertSignInButtonIsVisible(){
        actions.assertElementPresent("telerikAcademyTestForum.mainPage.signInButton");
    }

    public void clickSignInButton(){
        actions.clickElement("telerikAcademyTestForum.mainPage.signInButton");
    }

    public void assertCreateNewTopicButtonIsVisible(){
        actions.assertElementPresent("telerikAcademyTestForum.mainPage.newTopicButton");
    }



}
