package stepDefinitions;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class CreateTopicStepDefinition extends BaseStepDefinition {

    @Given("I log to the forum")
    public void navigateToPage() {
        telerikAcademyHomePage.navigateToPage();
    }

    @Given("I click add new topic button")
    public void clickAddNewTopic() {
        telerikAcademyHomePage.clickAddNewTopic();
    }

    @When("I type my topic $topic name")
    public void typeTopic(String topic) {
        telerikAcademyHomePage.fillTopicTitle(topic);
    }

    @When("I type my body $body description")
    public void typeBody(String body) {
        telerikAcademyHomePage.fillTopicBody(body);
    }

    @When("I select topic tag")
    public void selectTag() {
        telerikAcademyHomePage.clickTagDropdown();
        telerikAcademyHomePage.selectTag();
    }

    @When("I select category")
    public void selectCategory() {
        telerikAcademyHomePage.selectCategory();
    }

    @When("I click create topic button")
    public void clickCreateTopicButton() {
        telerikAcademyHomePage.clickCreateTopicButton();
    }
    @Then("I see my new topic")
    public void assertNewTopic(){
        telerikAcademyHomePage.assertTopicCreated();
    }

}
