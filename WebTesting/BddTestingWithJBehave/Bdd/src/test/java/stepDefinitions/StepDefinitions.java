package stepDefinitions;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class StepDefinitions extends BaseStepDefinition {

    @Given("Telerik Academy Logo is visible")
    public void logoIsVisible() {
        telerikAcademyMainPage.navigateToPage();
        telerikAcademyMainPage.assertLogoIsVisible();
    }

    @Given("Sign in button is visible")
    public void signInButtonIsVisible() {
        telerikAcademyMainPage.assertSignInButtonIsVisible();
    }

    @When("I click sign in button")
    public void clickSignInButton() {
        telerikAcademyMainPage.clickSignInButton();
    }

    @Then("I am redirected to login page")
    public void loginPage() {
        telerikAcademyLoginPage.assertPageUrl();
    }

    @Given("Email field is visible")
    public void emailFieldIsVisible() {
        telerikAcademyLoginPage.assertEmailIsVisible();
    }

    @Given("Password field is visible")
    public void passwordFieldIsVisible() {
        telerikAcademyLoginPage.assertPasswordVisible();
    }

    @Given("Login page sign in button is visible")
    public void loginPageSignInButtonIsVisible() {
        telerikAcademyLoginPage.assertSignInButtonVisible();
    }

    @When("I enter my email $text at the email field")
    public void typeEmail(String text) {
        telerikAcademyLoginPage.typeEmail(text);
    }

    @When("I enter my password $text at the password field")
    public void typePassword(String text) {
        telerikAcademyLoginPage.typePassword(text);
    }

    @When("I click login form sign in button")
    public void clickLoginSignInButton() {
        telerikAcademyLoginPage.clickSignInButton();
    }

}
