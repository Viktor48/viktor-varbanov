package stepDefinitions;

import com.telerikacademy.testframework.UserActions;
import org.jbehave.core.annotations.AfterStory;
import org.jbehave.core.annotations.BeforeStory;
import pages.bing.BingHomePage;
import pages.bing.BingResultsPage;
import pages.telerikAcademyTestForum.TelerikAcademyHomePage;
import pages.telerikAcademyTestForum.TelerikAcademyLoginPage;
import pages.telerikAcademyTestForum.TelerikAcademyMainPage;

public class BaseStepDefinition {

    UserActions userActions = new UserActions();
    TelerikAcademyMainPage telerikAcademyMainPage = new TelerikAcademyMainPage(userActions.getDriver());
    TelerikAcademyLoginPage telerikAcademyLoginPage = new TelerikAcademyLoginPage(userActions.getDriver());
    TelerikAcademyHomePage telerikAcademyHomePage =new TelerikAcademyHomePage(userActions.getDriver());

    @BeforeStory
    public void setUp() {
        UserActions.loadBrowser("telerikAcademyTestForum.mainPage.url");
    }

    @AfterStory
    public void tearDown() {
        UserActions.quitDriver();
    }
}
